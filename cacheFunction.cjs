// This function is to maintain cache. If cache containts that value which is already in cache than it stop the process and return the cache value.
function cacheFunction(cb) {
    let trackObj = []; // object which containt all the cache
    function invoke(cacheVal) { // infunction to call cb function
        let obj;
        for (let i = 0; i < cacheVal.length; i++) {
            if (trackObj.find((val) => val === cacheVal[i])) {//check trackobj contain the value or not
                break;
            }
            else {
                obj = cacheVal[i];// store the cache value in obj.
                trackObj.push(obj)// store all readed value in trackobj.
            }
        }
        return trackObj // at the end it return the trackobj;
    }
    return invoke; // call the closure function
}
module.exports = cacheFunction;