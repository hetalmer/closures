//CounterFacotry function Increment and Decrement the counter variable by 1 and return it.
//In this function increment function is in closure scope and function returns the object which contain two functions.
function counterFactory() {
    let counter = 0;
    function decrement() {//declare a decrement function 
        return --counter;//decrese the counter variable by 1
    }
    return {
        decrement,// return the decrement function
        increment() {// return Increment function
            return ++counter;
        }
    }
}
module.exports = counterFactory;
