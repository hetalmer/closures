//This function is invoke cb function but only n times. Invoke function is a closures function which call the cb function.
function limitFunctionCallCount(cb, n) {
    function invokeCb(x) { // crate a closure function which invoke cb
        if (n > 0) { // Cb invokes only n times
            n -= 1;
            return cb(x); // call cb function
        }
    }
    return invokeCb; // return the invokefunction
}
module.exports = limitFunctionCallCount;
