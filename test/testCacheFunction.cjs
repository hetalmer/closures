//This program to test the cacheFunction file.
const cacheFunction = require("../cacheFunction.cjs");
const t = cacheFunction((val) => val);// call cache function with cb 
x = [1, 2, 4, 5, 1, 7, 8];
console.log(t(x));// pass the cache value to cb function.
