// This file test the counterFactory function with increment and decrement the counter variable.
const counterFactory = require('../counterFactory.cjs');
//console.dir(counterFactory);
const objCounter = counterFactory();
//console.dir(objCounter);
console.log(objCounter.increment());
console.log(objCounter.decrement());
console.log(objCounter.increment());
console.log(objCounter.increment());
console.log(objCounter.decrement());
