//Created this file to test the testLimitFunctionCallCount function.
function isEven(num) { // cb function to check for even number
    return num % 2 === 0;
}
const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');
const invoke = limitFunctionCallCount(isEven, 3); // pass the cb and number of invokes to the function.
for (let i = 1; i <= 5; i++) {
    console.log(invoke(i)); // check the values for invoke
}